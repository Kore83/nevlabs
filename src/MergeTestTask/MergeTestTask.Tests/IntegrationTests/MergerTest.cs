﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace MergeTestTask.Tests.IntegrationTests
{
    [TestFixture]
    class MergerTest
    {
        private const string PathToTestData = "IntegrationTests\\TestData";

        [Test]
        public void TestData()
        {
            var merger = new MainMerger(new RecursiveLcsFinder());
            var outputGenerator = new OutputGenerator();

            foreach (var testFolder in Directory.GetDirectories(PathToTestData))
            {
                var result = merger.MergeFromFiles(testFolder + "\\Original.txt", testFolder + "\\Commit1.txt", testFolder + "\\Commit2.txt", outputGenerator).ToList();

                Assert.True(CompareStrings(result, File.ReadAllLines(testFolder + "\\Result.txt")), "Тест не прошел на данных из папки " + testFolder);
            }
        }

        private static bool CompareStrings(IList<String> first, IList<String> second)
        {
            if (first.Count != second.Count)
                return false;

            return !first.Where((t, i) => String.CompareOrdinal(t, second[i]) != 0).Any();
        }
    }
}
