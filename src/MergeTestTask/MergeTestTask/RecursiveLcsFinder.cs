﻿using System;
using MergeTestTask.Helper;

namespace MergeTestTask
{
    public class RecursiveLcsFinder : ILcsFinder
    {
        public LcsResult LongestCommonSubarray(int[] originalArray, int[] modifiedArray)
        {
            if (originalArray == null) throw new ArgumentNullException("originalArray");
            if (modifiedArray == null) throw new ArgumentNullException("modifiedArray");

            var original = ToHashedString(originalArray, StringModification.Deleted);
            var modified = ToHashedString(modifiedArray, StringModification.Added);

            LongestCommonSubarrayRecursive(original, modified, true);

            return new LcsResult() { Original = original, Modified = modified };
        }

        private static HashedString[] ToHashedString(int[] originalArray, StringModification modification)
        {
            var original = new HashedString[originalArray.Length];

            for (int i = 0; i < originalArray.Length; i++)
            {
                original[i] = new HashedString()
                    {
                        Hash = originalArray[i],
                        Modification = modification
                    };
            }
            return original;
        }

        private static void LongestCommonSubarrayRecursive(HashedString[] array1, HashedString[] array2, bool leftOriented)
        {
            if (array1.Length == 0 || array2.Length == 0)
                return;

            var prefix = leftOriented ? 0 : 1;

            var a = new int[array1.Length + 1, array2.Length + 1];
            int u = 0, v = 0;

            for (var i = 0; i < array1.Length; i++)
                for (var j = 0; j < array2.Length; j++)
                    if (array1[i].Hash == array2[j].Hash)
                    {
                        a[i + 1, j + 1] = a[i, j] + 1;
                        if (a[i + 1, j + 1] + prefix > a[u, v])
                        {
                            u = i + 1;
                            v = j + 1;
                        }
                    }

            var length = a[u, v];
            if (length == 0)
                return;

            SetUnmodified(array1, u - length, u - 1);
            SetUnmodified(array2, v - length, v - 1);

            LongestCommonSubarrayRecursive(array1.SubArray(0, u - length), array2.SubArray(0, v - length), true);
            LongestCommonSubarrayRecursive(array1.SubArray(u, array1.Length - u), array2.SubArray(v, array2.Length - v), false);
        }

        private static void SetUnmodified(HashedString[] array1, int startPos, int endPos)
        {
            for (int i = startPos; i <= endPos; i++)
            {
                array1[i].Modification = StringModification.Unmodified;
            }
        }
    }
}