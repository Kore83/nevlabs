using System;
using System.Collections.Generic;

namespace MergeTestTask
{
    public class OutputGenerator : IOutputGenerator
    {
        private static readonly string Conflict = "CONFLICT>";
        private static readonly string Deleted = "*������ �������*";

        public IEnumerable<string> Generate(IEnumerable<string> modificatedText1, IEnumerable<string> modificatedText2, IList<HashedString> diff1, IList<HashedString> diff2)
        {
            using (var enumerator1 = modificatedText1.GetEnumerator())
            {
                using (var enumerator2 = modificatedText2.GetEnumerator())
                {
                    var position1 = new Position(enumerator1, diff1);
                    var position2 = new Position(enumerator2, diff2);
                    do
                    {
                        switch (position1.Modification)
                        {
                            case StringModification.Unmodified:
                                switch (position2.Modification)
                                {
                                    case StringModification.Unmodified:
                                        yield return position1.ReturnLine();;
                                        break;
                                    case StringModification.Deleted:
                                        break;
                                    case StringModification.Added:
                                        yield return position2.ReturnLine();
                                        position2.Next();
                                        continue;
                                        break;
                                    default:
                                        throw new ArgumentOutOfRangeException();
                                }
                                break;
                            case StringModification.Deleted:
                                switch (position2.Modification)
                                {
                                    case StringModification.Unmodified:
                                        break;
                                    case StringModification.Deleted:
                                        break;
                                    case StringModification.Added:
                                        yield return Conflict + position2.ReturnLine();
                                        yield return Conflict + Deleted;
                                        break;
                                    default:
                                        throw new ArgumentOutOfRangeException();
                                }
                                break;
                            case StringModification.Added:
                                switch (position2.Modification)
                                {
                                    case StringModification.Unmodified:
                                        yield return position1.ReturnLine();
                                        position1.Next();
                                        continue;
                                        break;
                                    case StringModification.Deleted:
                                        yield return Conflict + position1.ReturnLine();
                                        yield return Conflict + Deleted;
                                        break;
                                    case StringModification.Added:
                                        if (position1.Hash == position2.Hash)
                                            yield return position1.ReturnLine();
                                        else
                                        {
                                            
                                            yield return Conflict + position1.ReturnLine();
                                            yield return Conflict + position2.ReturnLine();
                                        }
                                        break;
                                    default:
                                        throw new ArgumentOutOfRangeException();
                                }
                                break;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                        position1.Next();
                        position2.Next();
                    } while (position1.CollectionPosition < diff1.Count && position2.CollectionPosition < diff2.Count);

                    while (position1.CollectionPosition < diff1.Count)
                    {
                        if (position1.Modification != StringModification.Deleted)
                            yield return position1.ReturnLine();
                        
                        position1.Next();
                    }

                    while (position2.CollectionPosition < diff2.Count)
                    {
                        if (position2.Modification != StringModification.Deleted)
                            yield return position2.ReturnLine();

                        position2.Next();
                    }
                }
            }
        }

        private class Position
        {
            private readonly IEnumerator<string> enumerator;
            private readonly IList<HashedString> hashedStrings;
            private int collectionPosition;
            private int deleted;
            private int currentFilePosition;

            public Position(IEnumerator<String> enumerator, IList<HashedString> hashedStrings)
            {
                this.enumerator = enumerator;
                this.hashedStrings = hashedStrings;
                collectionPosition = 0;
                deleted = 0;
                currentFilePosition = -1;
            }

            public int Hash
            {
                get { return hashedStrings[CollectionPosition].Hash; }
            }

            public StringModification Modification
            {
                get { return hashedStrings[CollectionPosition].Modification; }
            }

            private int DesiredFilePosition 
            {
                get { return CollectionPosition - deleted; } 
            }

            public int CollectionPosition
            {
                get { return collectionPosition; }
            }

            public void Next()
            {
                if (hashedStrings[this.CollectionPosition].Modification == StringModification.Deleted)
                    this.deleted++;
                this.collectionPosition = this.CollectionPosition + 1;
            }

            public String ReturnLine()
            {
                if (currentFilePosition > DesiredFilePosition)
                {
                    currentFilePosition = 0;
                    enumerator.Reset();
                }

                for (var i = 0; i < DesiredFilePosition - currentFilePosition; i++)
                {
                    if (!enumerator.MoveNext())
                        return null;
                }

                currentFilePosition = DesiredFilePosition;
                return enumerator.Current;
            }
        }
    }
}