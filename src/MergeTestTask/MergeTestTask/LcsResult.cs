namespace MergeTestTask
{
    public class LcsResult
    {
        public HashedString[] Original { get; set; }
        public HashedString[] Modified { get; set; }
    }
}