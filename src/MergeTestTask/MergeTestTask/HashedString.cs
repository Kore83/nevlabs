using System.Globalization;

namespace MergeTestTask
{
    public class HashedString
    {
        public StringModification Modification { get; set; }
        public int Hash { get; set; }

        public override string ToString()
        {
            return string.Format("{0} {1}", Hash.ToString(CultureInfo.InvariantCulture), Modification.ToString());
        }
    }
}