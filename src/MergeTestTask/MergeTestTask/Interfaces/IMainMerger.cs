using System;
using System.Collections.Generic;

namespace MergeTestTask
{
    public interface IMainMerger
    {
        IEnumerable<string> MergeFromFiles(string pathToOriginalText, string pathToModificatedText1, string pathToModificatedText2, IOutputGenerator generator);
        IEnumerable<string> MergeFromStrings(string originalText, string modifiedText1, string modifiedText2, IOutputGenerator generator);
        IEnumerable<String> Merge(IEnumerable<string> originalText, IEnumerable<string> modificatedText1, IEnumerable<string> modificatedText2, IOutputGenerator generator);
    }
}