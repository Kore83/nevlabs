namespace MergeTestTask
{
    public interface ILcsFinder
    {
        LcsResult LongestCommonSubarray(int[] originalArray, int[] modifiedArray);
    }
}