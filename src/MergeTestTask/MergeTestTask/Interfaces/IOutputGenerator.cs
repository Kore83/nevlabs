﻿using System.Collections.Generic;

namespace MergeTestTask
{
    public interface IOutputGenerator
    {
        IEnumerable<string> Generate(IEnumerable<string> modificatedText1, IEnumerable<string> modificatedText2,
                                     IList<HashedString> diff1, IList<HashedString> diff2);
    }
}