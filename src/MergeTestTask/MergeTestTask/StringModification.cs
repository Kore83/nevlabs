namespace MergeTestTask
{
    public enum StringModification
    {
        Unmodified,
        Deleted,
        Added
    }
}