﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MergeTestTask
{
    public class MainMerger : IMainMerger
    {
        private readonly ILcsFinder lcsFinder;

        public MainMerger(ILcsFinder lcsFinder)
        {
            this.lcsFinder = lcsFinder;
        }

        public IEnumerable<string> MergeFromFiles(string pathToOriginalText, string pathToModificatedText1, string pathToModificatedText2, IOutputGenerator outputGenerator)
        {
            if (!File.Exists(pathToOriginalText))
                throw new FileNotFoundException("Файл не найден", pathToOriginalText);

            if (!File.Exists(pathToModificatedText1))
                throw new FileNotFoundException("Файл не найден", pathToModificatedText1);

            if (!File.Exists(pathToModificatedText2))
                throw new FileNotFoundException("Файл не найден", pathToModificatedText2);

            return Merge(File.ReadLines(pathToOriginalText),
                File.ReadLines(pathToModificatedText1),
                File.ReadLines(pathToModificatedText2),
                outputGenerator);
        }

        public IEnumerable<string> MergeFromStrings(string originalText, string modifiedText1, string modifiedText2, IOutputGenerator generator)
        {
            var separator = new[] {Environment.NewLine};
            return Merge(originalText.Split(separator, StringSplitOptions.None),
                         modifiedText1.Split(separator, StringSplitOptions.None),
                         modifiedText2.Split(separator, StringSplitOptions.None),
                         generator);
        }

        public IEnumerable<string> Merge(IEnumerable<string> originalText, IEnumerable<string> modificatedText1, IEnumerable<string> modificatedText2, IOutputGenerator generator)
        {
            if (originalText == null) throw new ArgumentNullException("originalText");
            if (modificatedText1 == null) throw new ArgumentNullException("modificatedText1");
            if (modificatedText2 == null) throw new ArgumentNullException("modificatedText2");

            var originalArray = originalText.Select(GetHashCode).ToArray();
            var firstModificatedText = modificatedText1.Select(GetHashCode).ToArray();
            var secondModificatedText = modificatedText2.Select(GetHashCode).ToArray();


            var result1 = this.lcsFinder.LongestCommonSubarray(originalArray, firstModificatedText);
            var result2 = this.lcsFinder.LongestCommonSubarray(originalArray, secondModificatedText);

            var diff1 = GetDiff(result1.Original, result1.Modified);
            var diff2 = GetDiff(result2.Original, result2.Modified);

            return generator.Generate(modificatedText1, modificatedText2, diff1, diff2);
        }

        private static List<HashedString> GetDiff(HashedString[] originalArray, HashedString[] firstModificatedText)
        {
            var diff = new List<HashedString>(firstModificatedText.Length);

            var readedOrig = 0;
            var readedModified = 0;

            do
            {
                diff.AddRange(GetModified(originalArray, firstModificatedText, ref readedOrig, ref readedModified));
                diff.AddRange(GetEquals(originalArray, firstModificatedText, ref readedOrig, ref readedModified,
                    s => s.Modification == StringModification.Unmodified));
            } while (readedOrig < originalArray.Length || readedModified < firstModificatedText.Length);

            return diff;
        }

        private static IEnumerable<HashedString> GetModified(HashedString[] array1, HashedString[] array2, ref int pos1, ref int pos2)
        {
            var result = new List<HashedString>();

            while (pos1 < array1.Length && array1[pos1].Modification != StringModification.Unmodified)
            {
                result.Add(array1[pos1++]);
            }

            while (pos2 < array2.Length && array2[pos2].Modification != StringModification.Unmodified)
            {
                result.Add(array2[pos2++]);
            }

            return result;
        }

        private static IEnumerable<HashedString> GetEquals(HashedString[] array1, HashedString[] array2, ref int pos1, ref int pos2, Func<HashedString, bool> predicate)
        {
            var result = new List<HashedString>();
            while (pos1 < array1.Length && pos2 < array2.Length)
            {
                if (!predicate(array1[pos1]) || !predicate(array2[pos2]))
                    break;

                result.Add(array2[pos2++]);
                pos1++;
            } 

            return result;
        }

        private static int GetHashCode(string s)
        {
            return s.Trim().GetHashCode();
        }
    }
}