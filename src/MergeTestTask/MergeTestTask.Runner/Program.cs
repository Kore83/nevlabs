﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MergeTestTask.Runner
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 4)
            {
                Console.WriteLine("Недостаточное количество параметров");
                return;
            }
            try
            {
                var merger = new MainMerger(new RecursiveLcsFinder());
                var outputGenerator = new OutputGenerator();
                var result = merger.MergeFromFiles(args[0], args[1], args[2], outputGenerator);

                File.WriteAllLines(args[3], result);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + ex.StackTrace);
            }
        }
    }
}
